/***** eltl2pml : lex.c *****/

/* Written by Denis Oddoux, LIAFA, France                                 */
/* Copyright (c) 2001  Denis Oddoux                                       */
/* Modified by Paul Gastin, LSV, France                                   */
/* Copyright (c) 2007  Paul Gastin                                        */
/*                                                                        */
/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/
/*                                                                        */
/* Based on the translation algorithm by Gastin and Oddoux,               */
/* presented at the 13th International Conference on Computer Aided       */
/* Verification, CAV 2001, Paris, France.                                 */
/* Proceedings - LNCS 2102, pp. 53-65                                     */
/*                                                                        */
/* Send bug-reports and/or questions to Paul Gastin                       */
/* http://www.lsv.ens-cachan.fr/~gastin                                   */
/*                                                                        */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include "config.h"
#include <stdlib.h>
#include <ctype.h>
#include "eltl2pml.h"

static Symbol	*symtab[Nhash+1];
static int	tl_lex(void);
int (*tl_yylex)() = tl_lex;

extern YYSTYPE	tl_yylval;
static char	yytext[2048];

#define Token(y)        tl_yylval = tl_nn(y,ZN,ZN); return y

static int
isalnum_(int c)
{       return (isalnum(c) || c == '_'); //LPJ:ojo con la sintaxis que quiero usar con _{}
}

static int
hash(char *s)
{       int h=0;

        while (*s)
        {       h += *s++;
                h <<= 1;
                if (h&(Nhash+1))
                        h |= 1;
        }
        return h&Nhash;
}

static void
getword(int first, int (*tst)(int))
{	int i=0; char c;

	yytext[i++]= (char ) first;
	while (tst(c = tl_Getchar()))
		yytext[i++] = c;
	yytext[i] = '\0';
	tl_UnGetchar();
}

static int
follow(int tok, int ifyes, int ifno)
{	int c;
	char buf[32];
	extern int tl_yychar;

	if ((c = tl_Getchar()) == tok)
		return ifyes;
	tl_UnGetchar();
	tl_yychar = c;
	sprintf(buf, "expected '%c'", tok);
	tl_yyerror(buf);	/* no return from here */
	return ifno;
}


static int
tl_lex(void)
{	
	int c;
	do {
		c = tl_Getchar();
		yytext[0] = (char ) c;
		yytext[1] = '\0';

		if (c <= 0)
		{	Token(';');
		}

	} while (c == ' ');	/* '\t' is removed in tl_main.c */

	if (islower(c))
	{	getword(c, isalnum_);
		if (strcmp("true", yytext) == 0)
		{	Token(TRUE);
		}
		if (strcmp("false", yytext) == 0)
		{	Token(FALSE);
		}
		if(strcmp("exists", yytext) == 0){
			do{
				c = tl_Getchar();
			}while(c==' ');

			getword(c, isalnum_); //la variable cuantificada puede tener un nombre de varios caracteres
			
			tl_yylval = tl_nn(EXISTS,ZN,ZN);
			tl_yylval->sym = tl_lookup(yytext);
			return EXISTS;
		} else if(strcmp("forall", yytext) == 0){
			do{
				c = tl_Getchar();
			}while(c==' ');
			getword(c, isalnum_); //la variable cuantificada puede tener un nombre de varios caracteres
			
			tl_yylval = tl_nn(FORALL,ZN,ZN);
			tl_yylval->sym = tl_lookup(yytext);
			return FORALL;
		} else{
			tl_yylval = tl_nn(PREDICATE,ZN,ZN);		
			tl_yylval->sym = tl_lookup(yytext);
			return PREDICATE;
		}		
	}
	
	if(c == 'E') 
	{  /* E_{ev1,ev2} == EVENTUALLY_PQ o  E_{ev1} == EVENTUALLY_P*/
		c = tl_Getchar();
		if(c!= '_')
		{	tl_UnGetchar();
			tl_yyerror("1) expected '_{ev1,ev2}' or '_{ev1}'");
		}
		c = tl_Getchar();
		if(c!= '{')
		{	tl_UnGetchar();
			tl_yyerror("2) expected '_{ev1,ev2}' or '_{ev1}'");
		}
		//LPJ: leer una palabra
		c = tl_Getchar();
		getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev1		
		Symbol *ev1 = tl_allocate(yytext);
		
		c = tl_Getchar();
		if(c=='(') //evento con parametros
		{ 		
			Symbol *aux = NULL;
			c = tl_Getchar();			
			while(c!=')'){
				getword(c, isalnum_);
				if(aux == NULL){
					ev1->params = tl_allocate(yytext);
					aux = ev1;					
				}else{
					aux->params = tl_allocate(yytext);					
				}
				aux->next = NULL;				 
				aux = aux->params;
				
				c = tl_Getchar();
				if(c==','){
					c = tl_Getchar();
				}else if(c!= ',' && c!=')'){
					tl_UnGetchar();
					tl_yyerror("lt_lex expected 'ev1(p1,p2,...)'");
				}
				
			}
			
			c = tl_Getchar();
		}
		if(c== ',')
		{	
			tl_yylval = tl_nn(EVENTUALLY_PQ,ZN,ZN);
			tl_yylval->sym = ev1;
			c = tl_Getchar();

			getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev2
			//tl_yylval->sym->next = malloc(sizeof(struct Symbol));
			tl_yylval->sym->next = tl_allocate(yytext);
			c = tl_Getchar();
			if(c=='(') //evento con parametros
			{ 			
				Symbol *aux = NULL;
				c = tl_Getchar();			
				while(c!=')'){
					getword(c, isalnum_);
					if(aux == NULL){
						tl_yylval->sym->next->params = tl_allocate(yytext);
						aux = tl_yylval->sym->next;					
					}else{
						aux->params = tl_allocate(yytext);					
					}
					aux->next = NULL;				 
					aux = aux->params;
				
					c = tl_Getchar();
					if(c==','){
						c = tl_Getchar();
					}else if(c!= ',' && c!=')'){
						tl_UnGetchar();
						tl_yyerror("lt_lex expected 'ev2(p1,p2,...)'");
					}				
				}
				c = tl_Getchar();			
			}	
			if(c!= '}'){	
				tl_UnGetchar();
				tl_yyerror("3) expected '_{ev1,ev2}'");
			}
			
			return EVENTUALLY_PQ;
		}else if(c=='}')
		{	tl_yylval = tl_nn(EVENTUALLY_P,ZN,ZN);
			tl_yylval->sym = ev1; 
			return EVENTUALLY_P;
		}else{
			tl_UnGetchar();
			tl_yyerror("4) expected '_{ev1,ev2}' or '_{ev1}'");
		}
	}
	if(c == 'A') 
	{  /* A_{ev1,ev2} == ALWAYS_PQ o A_{ev1} == ALWAYS_P*/
		c = tl_Getchar();
		if(c!= '_')
		{	tl_UnGetchar();
			tl_yyerror("expected '_{ev1,ev2}' or '_{ev1}'");
		}
		c = tl_Getchar();
		if(c!= '{')
		{	tl_UnGetchar();
			tl_yyerror("expected '_{ev1,ev2}' or '_{ev1}'");
		}
		//LPJ: leer una palabra
		c = tl_Getchar();
		getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev1
		Symbol *ev1 = tl_allocate(yytext);
		
		c = tl_Getchar();
		if(c=='(') //evento con parametros
		{ 		
			Symbol *aux = NULL;
			c = tl_Getchar();			
			while(c!=')'){
				getword(c, isalnum_);
				if(aux == NULL){
					ev1->params = tl_allocate(yytext);
					aux = ev1;					
				}else{
					aux->params = tl_allocate(yytext);					
				}
				aux->next = NULL;				 
				aux = aux->params;
				
				c = tl_Getchar();
				if(c==','){
					c = tl_Getchar();
				}else if(c!= ',' && c!=')'){
					tl_UnGetchar();
					tl_yyerror("lt_lex expected 'ev1(p1,p2,...)'");
				}
				
			}
			
			c = tl_Getchar();
		}
		if(c== ','){				
			tl_yylval = tl_nn(ALWAYS_PQ,ZN,ZN);
			//tl_yylval->sym = tl_lookup(yytext);
			tl_yylval->sym = ev1;
			c = tl_Getchar();

			getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev2
			//tl_yylval->sym->next = malloc(sizeof(struct Symbol));
			tl_yylval->sym->next = tl_allocate(yytext);
			//tl_yylval->sym->next = tl_lookup(yytext);
			c = tl_Getchar();
			if(c=='(') //evento con parametros
			{ 			
				Symbol *aux = NULL;
				c = tl_Getchar();			
				while(c!=')'){
					getword(c, isalnum_);
					if(aux == NULL){
						tl_yylval->sym->next->params = tl_allocate(yytext);
						aux = tl_yylval->sym->next;					
					}else{
						aux->params = tl_allocate(yytext);					
					}
					aux->next = NULL;				 
					aux = aux->params;
				
					c = tl_Getchar();
					if(c==','){
						c = tl_Getchar();
					}else if(c!= ',' && c!=')'){
						tl_UnGetchar();
						tl_yyerror("lt_lex expected 'ev2(p1,p2,...)'");
					}				
				}
				c = tl_Getchar();			
			}
			if(c!= '}'){	tl_UnGetchar();
				tl_yyerror("expected '_{ev1,ev2}' ");
			}
			return ALWAYS_PQ;
		}else if(c=='}')
		{	
			tl_yylval = tl_nn(ALWAYS_P,ZN,ZN);
			tl_yylval->sym = tl_lookup(yytext); 			
			return ALWAYS_P;
		}else{
			tl_UnGetchar();
			tl_yyerror("expected '_{ev1}'");
		}
	}
	if(c == 'U') 
	{  /* U_{ev1,ev2} == UNTIL_PQ o U_{ev1} == UNTIL_P*/
		c = tl_Getchar();
		if(c!= '_')
		{	tl_UnGetchar();
			tl_yyerror("expected '_{ev1,ev2}' or '_{ev1}'");
		}
		c = tl_Getchar();
		if(c!= '{')
		{	tl_UnGetchar();
			tl_yyerror("expected '_{ev1,ev2}' or '_{ev1}'");
		}
		//LPJ: leer una palabra
		c = tl_Getchar();
		getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev1
		Symbol *ev1 = tl_allocate(yytext);
		

		c = tl_Getchar();
		if(c=='(') //evento con parametros
		{ 		
			Symbol *aux = NULL;
			c = tl_Getchar();			
			while(c!=')'){
				getword(c, isalnum_);
				if(aux == NULL){
					ev1->params = tl_allocate(yytext);
					aux = ev1;					
				}else{
					aux->params = tl_allocate(yytext);					
				}
				aux->next = NULL;				 
				aux = aux->params;
				
				c = tl_Getchar();
				if(c==','){
					c = tl_Getchar();
				}else if(c!= ',' && c!=')'){
					tl_UnGetchar();
					tl_yyerror("lt_lex expected 'ev1(p1,p2,...)'");
				}
				
			}
			
			c = tl_Getchar();
		}
		if(c== ','){				
			tl_yylval = tl_nn(UNTIL_PQ,ZN,ZN);
			//tl_yylval->sym = tl_lookup(yytext);
			tl_yylval->sym = ev1;
			c = tl_Getchar();
			getword(c, isalnum_); // actualiza yytext -> habría que guardar en algún sitio el ev2
			//tl_yylval->sym->next = malloc(sizeof(struct Symbol));
			//tl_yylval->sym->next = tl_lookup(yytext);
			tl_yylval->sym->next = tl_allocate(yytext);

			c = tl_Getchar();
			if(c=='(') //evento con parametros
			{ 			
				Symbol *aux = NULL;
				c = tl_Getchar();			
				while(c!=')'){
					getword(c, isalnum_);
					if(aux == NULL){
						tl_yylval->sym->next->params = tl_allocate(yytext);
						aux = tl_yylval->sym->next;					
					}else{
						aux->params = tl_allocate(yytext);					
					}
					aux->next = NULL;				 
					aux = aux->params;
				
					c = tl_Getchar();
					if(c==','){
						c = tl_Getchar();
					}else if(c!= ',' && c!=')'){
						tl_UnGetchar();
						tl_yyerror("lt_lex expected 'ev2(p1,p2,...)'");
					}				
				}
				c = tl_Getchar();			
			}
			if(c!= '}'){	tl_UnGetchar();
				tl_yyerror("expected '_{ev1,ev2}' ");
			}
			return UNTIL_PQ;
		}else if(c=='}')
		{	
			tl_yylval = tl_nn(UNTIL_P,ZN,ZN);
			tl_yylval->sym = tl_lookup(yytext); 			
			return UNTIL_P;
		}else{
			tl_UnGetchar();
			tl_yyerror("expected '_{ev1}'");
		}
	}
	if (c == '<')
	{	c = tl_Getchar();		
		if (c != '-')
		{	tl_UnGetchar();
			tl_yyerror("expected '<>' or '<->'");
		}
		c = tl_Getchar();
		if (c == '>')
		{	Token(EQUIV);
		}
		tl_UnGetchar();
		tl_yyerror("expected '<->'");
	}

	switch (c) {
	case '/' : c = follow('\\', AND, '/'); break;
	case '\\': c = follow('/', OR, '\\'); break;
	case '&' : c = follow('&', AND, '&'); break;
	case '|' : c = follow('|', OR, '|'); break;
	case '-' : c = follow('>', IMPLIES, '-'); break;
	case '!' : c = NOT; break;
	//case 'U' : c = U_OPER; break;
	
	default  : break;
	}
	Token(c);
}

Symbol *
tl_lookup(char *s)
{	Symbol *sp;
	int h = hash(s);

	for (sp = symtab[h]; sp; sp = sp->next)
		if (strcmp(sp->name, s) == 0)
			return sp;

	sp = (Symbol *) tl_emalloc(sizeof(Symbol));
	sp->name = (char *) tl_emalloc(strlen(s) + 1);
	strcpy(sp->name, s);
	sp->next = symtab[h];
	symtab[h] = sp;
	sp->params = NULL; //lpj

	return sp;
}

Symbol *
tl_allocate(char *s)
{	Symbol *sp;
	int h = hash(s);
	
	sp = (Symbol *) tl_emalloc(sizeof(Symbol));
	sp->name = (char *) tl_emalloc(strlen(s) + 1);
	strcpy(sp->name, s);
	sp->next = NULL;
	//sp->next = symtab[h];
	//symtab[h] = sp;
	sp->params = NULL; //lpj

	return sp;
}

Symbol *
getsym(Symbol *s)
{	Symbol *n = (Symbol *) tl_emalloc(sizeof(Symbol));

	n->name = s->name;
	return n;
}
