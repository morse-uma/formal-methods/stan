/***** ltl2ba : trans.c *****/
/* eltl2pml is freely distributed as part of STAN tool                    */
/* eltl2pml is an adaptation ltl2ba done by Laura Panizo, UMA, Malaga     */
/**************************************************************************/
/* Most of the code in this file was taken from ltl2ba                    */
/* Written by Denis Oddoux, LIAFA, France                                 */
/* Copyright (c) 2001  Denis Oddoux                                       */
/* Modified by Paul Gastin, LSV, France                                   */
/* Copyright (c) 2007  Paul Gastin                                        */
/*                                                                        */
/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/
/*                                                                        */
/* Based on the translation algorithm by Gastin and Oddoux,               */
/* presented at the 13th International Conference on Computer Aided       */
/* Verification, CAV 2001, Paris, France.                                 */
/* Proceedings - LNCS 2102, pp. 53-65                                     */
/*                                                                        */
/* Send bug-reports and/or questions to Paul Gastin                       */
/* http://www.lsv.ens-cachan.fr/~gastin                                   */
/*                                                                        */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include "config.h"
#include "eltl2pml.h"

extern int tl_verbose, tl_terse, tl_errs;
extern FILE	*tl_out;

int	Stack_mx=0, Max_Red=0, Total=0;
static char	dumpbuf[2048];

#ifdef NXT
int
only_nxt(Node *n)
{
        switch (n->ntyp) {
        case OR:
        case AND:
                return only_nxt(n->rgt) && only_nxt(n->lft);
        default:
                return 0;
        }
}
#endif

int
dump_cond(Node *pp, Node *r, int first)
{       Node *q;
        int frst = first;

        if (!pp) return frst;

        q = dupnode(pp);
        q = rewrite(q);

        if (q->ntyp == PREDICATE
        ||  q->ntyp == NOT
        ||  q->ntyp == FALSE)
        {       if (!frst) fprintf(tl_out, " && ");
                dump(q);
                frst = 0;
       } else  if (q->ntyp == AND)
        {
                frst = dump_cond(q->lft, r, frst);
                frst = dump_cond(q->rgt, r, frst);
        }

        return frst;
}

static void
sdump(Node *n)
{       char buffer[50];
	switch (n->ntyp) {
	case PREDICATE:	strcat(dumpbuf, n->sym->name);
			break;
        case UNTIL_PQ: 
                sprintf(buffer, "U_{%s,%s}", n->sym->name, n->sym->next->name); 
                strcat(dumpbuf, buffer);
                goto common2;
        case UNTIL_P: 
                sprintf(buffer, "U_{%s}", n->sym->name); 
                strcat(dumpbuf, buffer);
                goto common2;
        case ALWAYS_PQ: 
                sprintf(buffer, "A_{%s,%s}", n->sym->name, n->sym->next->name); 
                strcat(dumpbuf, buffer);
                goto common1;
        case ALWAYS_P: 
                sprintf(buffer, "A_{%s}", n->sym->name); 
                strcat(dumpbuf, buffer);
                goto common1;
        case EVENTUALLY_PQ: 
                sprintf(buffer, "E_{%s,%s}", n->sym->name, n->sym->next->name); 
                strcat(dumpbuf, buffer);
                goto common1;
        case EVENTUALLY_P: 
                sprintf(buffer, "E_{%s}", n->sym->name); 
                strcat(dumpbuf, buffer);
                goto common1;
	case OR:	strcat(dumpbuf, "|");
			goto common2;
	case AND:	strcat(dumpbuf, "&");
common2:		sdump(n->rgt);
common1:		sdump(n->lft);
			break;

	case NOT:	strcat(dumpbuf, "!");
			goto common1;
	case TRUE:	strcat(dumpbuf, "T");
			break;
	case FALSE:	strcat(dumpbuf, "F");
			break;
	default:	strcat(dumpbuf, "?");
			break;
	}
}

Symbol *
DoDump(Node *n)
{
	if (!n) return ZS;

	if (n->ntyp == PREDICATE)
		return n->sym;

	dumpbuf[0] = '\0';
	sdump(n);
	return tl_lookup(dumpbuf);
}
