/***** eltl2pml : parse.c *****/
/* eltl2pml is freely distributed as part of STAN tool                    */
/* eltl2pml is an adaptation ltl2ba done by Laura Panizo, UMA, Malaga     */
/**************************************************************************/
/* Most of the code in this file was taken from ltl2ba                    */
/* Written by Denis Oddoux, LIAFA, France                                 */
/* Copyright (c) 2001  Denis Oddoux                                       */
/* Modified by Paul Gastin, LSV, France                                   */
/* Copyright (c) 2007  Paul Gastin                                        */
/*                                                                        */
/* This program is free software; you can redistribute it and/or modify   */
/* it under the terms of the GNU General Public License as published by   */
/* the Free Software Foundation; either version 2 of the License, or      */
/* (at your option) any later version.                                    */
/*                                                                        */
/* This program is distributed in the hope that it will be useful,        */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/* GNU General Public License for more details.                           */
/*                                                                        */
/* You should have received a copy of the GNU General Public License      */
/* along with this program; if not, write to the Free Software            */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA*/
/*                                                                        */
/* Based on the translation algorithm by Gastin and Oddoux,               */
/* presented at the 13th International Conference on Computer Aided       */
/* Verification, CAV 2001, Paris, France.                                 */
/* Proceedings - LNCS 2102, pp. 53-65                                     */
/*                                                                        */
/* Send bug-reports and/or questions to Paul Gastin                       */
/* http://www.lsv.ens-cachan.fr/~gastin                                   */
/*                                                                        */
/* Some of the code in this file was taken from the Spin software         */
/* Written by Gerard J. Holzmann, Bell Laboratories, U.S.A.               */

#include "config.h"
#include "eltl2pml.h"

extern int	tl_verbose, tl_simp_log;
extern FILE	*tl_out; /* Lpj: para escribir en fichero de salida? */

int	tl_yychar = 0;
YYSTYPE	tl_yylval;

static Node	*tl_formula(void);
static Node	*tl_factor(void);
static Node	*tl_level(int);


static int	prec[2][4] = {
	{ UNTIL_PQ, UNTIL_P, 0, 0},  /* left associative */
	{ OR, AND, IMPLIES, EQUIV },	/* left associative */
};

static int
implies(Node *a, Node *b)
{
  return
    (isequal(a,b) ||
     b->ntyp == TRUE ||
     a->ntyp == FALSE ||
     (b->ntyp == AND && implies(a, b->lft) && implies(a, b->rgt)) ||
     (a->ntyp == OR && implies(a->lft, b) && implies(a->rgt, b)) ||
     (a->ntyp == AND && (implies(a->lft, b) || implies(a->rgt, b))) ||
     (b->ntyp == OR && (implies(a, b->lft) || implies(a, b->rgt))));
}



static Node *
bin_simpler(Node *ptr)
{	Node *a, *b;

	if (ptr)
	switch (ptr->ntyp) {
	
	case IMPLIES:
		if (implies(ptr->lft, ptr->rgt))
		  {	ptr = True;
			break;
		}
		ptr = tl_nn(OR, Not(ptr->lft), ptr->rgt);
		ptr = rewrite(ptr);
		break;
	case EQUIV:
		if (implies(ptr->lft, ptr->rgt) &&
		    implies(ptr->rgt, ptr->lft))
		  {	ptr = True;
			break;
		}
		a = rewrite(tl_nn(AND,
			dupnode(ptr->lft),
			dupnode(ptr->rgt)));
		b = rewrite(tl_nn(AND,
			Not(ptr->lft),
			Not(ptr->rgt)));
		ptr = tl_nn(OR, a, b);
		ptr = rewrite(ptr);
		break;
	case AND:
		if (isequal(ptr->lft, ptr->rgt)	// (p && p) == p 
		||  ptr->rgt->ntyp == FALSE	// (p && F) == F 
		||  ptr->lft->ntyp == TRUE	// (T && p) == p 
		||  implies(ptr->rgt, ptr->lft))// NEW 
		{	ptr = ptr->rgt;
			break;
		}	
		if (ptr->rgt->ntyp == TRUE	// (p && T) == p 
		||  ptr->lft->ntyp == FALSE	// (F && p) == F 
		||  implies(ptr->lft, ptr->rgt))// NEW 
		{	ptr = ptr->lft;
			break;
		}
		// NEW
		if (implies(ptr->lft, 
			    push_negation(tl_nn(NOT, dupnode(ptr->rgt), ZN)))
		 || implies(ptr->rgt, 
			    push_negation(tl_nn(NOT, dupnode(ptr->lft), ZN))))
		{       ptr = False;
		        break;
		}
		break;

	case OR:
		if (isequal(ptr->lft, ptr->rgt)	// (p || p) == p 
		||  ptr->rgt->ntyp == FALSE	// (p || F) == p 
		||  ptr->lft->ntyp == TRUE	// (T || p) == T 
		||  implies(ptr->rgt, ptr->lft))// NEW 
		{	ptr = ptr->lft;
			break;
		}	
		if (ptr->rgt->ntyp == TRUE	// (p || T) == T 
		||  ptr->lft->ntyp == FALSE	// (F || p) == p 
		||  implies(ptr->lft, ptr->rgt))// NEW 
		{	ptr = ptr->rgt;
			break;
		}
		// NEW
		if (implies(push_negation(tl_nn(NOT, dupnode(ptr->rgt), ZN)),
			    ptr->lft)
		 || implies(push_negation(tl_nn(NOT, dupnode(ptr->lft), ZN)),
			    ptr->rgt))
		{       ptr = True;
		        break;
		}
		break;
	}
	return ptr;
}

static Node *
bin_minimal(Node *ptr)
{       if (ptr)
	switch (ptr->ntyp) {
	case IMPLIES:
		return tl_nn(OR, Not(ptr->lft), ptr->rgt);
	case EQUIV:
		return tl_nn(OR, 
			     tl_nn(AND,dupnode(ptr->lft),dupnode(ptr->rgt)),
			     tl_nn(AND,Not(ptr->lft),Not(ptr->rgt)));
	}
	return ptr;
}

static Node *
tl_factor(void)
{	Node *ptr = ZN;

	switch (tl_yychar) {
	case '(':
		ptr = tl_formula();
		if (tl_yychar != ')')
			tl_yyerror("expected ')'");
		tl_yychar = tl_yylex();
		goto simpl;
	case NOT:
		ptr = tl_yylval;
		tl_yychar = tl_yylex();
		ptr->lft = tl_factor();
		ptr = push_negation(ptr);
		goto simpl;
	case ALWAYS_PQ: 
		ptr = tl_nn(ALWAYS_PQ, ZN, ZN);
		ptr->sym = tl_yylval->sym; 		
		tl_yychar = tl_yylex();		
		ptr->lft = tl_factor();
		break;		
	case ALWAYS_P:
		ptr = tl_nn(ALWAYS_P, ZN, ZN);
		ptr->sym = tl_yylval->sym;
		tl_yychar = tl_yylex();
		ptr->lft = tl_factor();
		break;		
	case EVENTUALLY_PQ: 
		ptr = tl_nn(EVENTUALLY_PQ, ZN, ZN);
		ptr->sym = tl_yylval->sym; 		
		tl_yychar = tl_yylex();		
		ptr->lft = tl_factor();
		break;		
	case EVENTUALLY_P:
		ptr = tl_nn(EVENTUALLY_P, ZN, ZN);
		ptr->sym = tl_yylval->sym; 		
		tl_yychar = tl_yylex();		
		ptr->lft = tl_factor();
		break;
	simpl:
		if (tl_simp_log) 
		  ptr = bin_simpler(ptr);
		break;
	case EXISTS:
		ptr = tl_nn(EXISTS, ZN, ZN);
		ptr->sym = tl_yylval->sym; 		
		tl_yychar = tl_yylex();		
		ptr->lft = tl_factor();
		break;
	case FORALL:
		ptr = tl_nn(FORALL, ZN, ZN);
		ptr->sym = tl_yylval->sym; 		
		tl_yychar = tl_yylex();		
		ptr->lft = tl_factor();
		break;
	case PREDICATE:
		ptr = tl_yylval;
		tl_yychar = tl_yylex();
		break;
	case TRUE:
	case FALSE:
		ptr = tl_yylval;
		tl_yychar = tl_yylex();
		break;
	}
	if (!ptr) tl_yyerror("expected predicate");
#if 1
	printf("factor:	");
	tl_explain(ptr->ntyp);
	printf("\n");
#endif
	return ptr;
}

static Node *
tl_level(int nr)
{	int i; Node *ptr = ZN;

	if (nr < 0)
		return tl_factor();

	ptr = tl_level(nr-1);
again:
	for (i = 0; i < 4; i++)
		if (tl_yychar == prec[nr][i])
		{	Symbol *aux = tl_yylval->sym;
			
			tl_yychar = tl_yylex();
			ptr = tl_nn(prec[nr][i],
				ptr, tl_level(nr-1));
			if(prec[nr][i]== UNTIL_PQ ||prec[nr][i]== UNTIL_P){
					ptr->sym = aux;
			}	
			if(tl_simp_log) ptr = bin_simpler(ptr);
			else ptr = bin_minimal(ptr);
			goto again;
		}
	if (!ptr) tl_yyerror("syntax error");
#if 0
	printf("level %d:	", nr);
	tl_explain(ptr->ntyp);
	printf("\n");
#endif
	return ptr;
}

static Node *
tl_formula(void){	
	tl_yychar = tl_yylex();
	return tl_level(1);	/* 2 precedence levels, 1 and 0 */	
}

void generate_run(Node *n, int nestLevel, int id){
	
switch(n->ntyp){
			case ALWAYS_P:
				fprintf(tl_out,"run ALWAYS_P(%d,%d,cmC,rdC,%s);\n", nestLevel, id, n->sym->name);								
				break;
			case ALWAYS_PQ:
				fprintf(tl_out,"run ALWAYS_PQ(%d,%d,cmC,rdC, %s, %s);\n", nestLevel, id, n->sym->name, n->sym->next->name );				
				break;
			case EVENTUALLY_P:
				fprintf(tl_out,"run EVENTUALLY_P(%d,%d,cmC,rdC,%s);\n", nestLevel, id, n->sym->name );				
				break;
			case EVENTUALLY_PQ:
				fprintf(tl_out,"run EVENTUALLY_PQ(%d,%d,cmC,rdC,%s, %s);\n", nestLevel, id, n->sym->name, n->sym->next->name );				
				break;
			case UNTIL_P:								
				fprintf(tl_out,"run UNTIL_P(%d,%d,cmC,rdC, %s);\n", nestLevel, id, n->sym->name );				
				break;
			case UNTIL_PQ:				
				fprintf(tl_out,"run UNTIL_PQ(%d,%d,cmC,rdC,%s, %s);\n", nestLevel, id, n->sym->name, n->sym->next->name );				
				break;
			case NOT:
				fprintf(tl_out,"run NOT(%d,%d,cmC,rdC);\n", nestLevel, id);				
				break;
			case OR:									
				fprintf(tl_out,"run OR(%d,%d,cmC,rdC);\n", nestLevel, id);
				break;
			case AND:
				fprintf(tl_out,"run AND(%d,%d,cmC,rdC);\n", nestLevel, id);
				break;
			case TRUE:
				fprintf(tl_out,"run TRUE(%d,%d,cmC,rdC);\n", nestLevel, id);
				break;
			case PREDICATE:
				fprintf(tl_out,"run %s(%d,%d,cmC,rdC);\n", n->sym->name, nestLevel, id);
				break;
			case EXISTS:
				fprintf(tl_out,"run EXISTS(%d,%d,cmC,rdC, %s);\n", nestLevel, id, n->sym->name);
				break;
			case FORALL:
				fprintf(tl_out,"run FORALL(%d,%d,cmC,rdC, %s);\n", nestLevel, id, n->sym->name);
				break;
			default:
				printf("eLTL operator not implemented\n");
				alldone(1);
				
		} 
}
int generate_runChild(Node *n, int fId){	
	int nextfId = fId+1;	
	switch(n->ntyp){
			case ALWAYS_P:
			case ALWAYS_PQ:
			case EVENTUALLY_P:
			case EVENTUALLY_PQ:
			case EXISTS:
			case FORALL:
			case NOT:				
				fprintf(tl_out,"\t:: f== %d &&  c ==1 -> ",fId);				
				generate_run(n->lft,nextfId, 1);
				fprintf(tl_out,"\t:: f== %d &&  c ==2 -> assert(false);\n",fId);	
				nextfId = generate_runChild(n->lft, nextfId);
				break;						
			case UNTIL_P:
			case UNTIL_PQ:
			case OR:
			case AND:				
				fprintf(tl_out,"\t:: f== %d &&  c ==1 -> ",fId);	
				generate_run(n->lft,nextfId, 1);
				nextfId = generate_runChild(n->lft, nextfId);
				
				fprintf(tl_out, "\t:: f== %d &&  c ==2 -> ",fId);
				generate_run(n->rgt,nextfId, 2);
				nextfId = generate_runChild(n->rgt,nextfId);
				
				break;
			
			case TRUE:				
			case PREDICATE:				
				break;
			default:
				printf("eLTL operator not implemented\n");
				alldone(1);
		}
		return nextfId;
}
int generate_varIdsRec(Node *n, int vId){
	Node *aux = n;
	while(aux!= NULL){
		if(aux->ntyp == EXISTS || aux->ntyp == FORALL){
			fprintf(tl_out, "#define %s %d\n", aux->sym->name, vId);
			vId++;
			aux = aux->lft;			
		}else if(aux->ntyp == AND || aux->ntyp == OR || aux->ntyp == UNTIL_P || aux->ntyp == UNTIL_PQ){
			vId = generate_varIdsRec(aux->lft, vId);
			return generate_varIdsRec(aux->rgt, vId);
		}else{
			aux = aux->lft;
		}
		
	}
	return vId;

}
int generate_initQVarsRec(Node *n, int vId){
	Node *aux = n;
	if(aux!=NULL){
		if(aux->ntyp == EXISTS || aux->ntyp == FORALL){			
			Node * aux2 = aux->lft;
			while(aux2 != NULL){
				//aux2->sym es el nombre del evento
				//aux2->sym->params es la lista de variables asociadas al evento
				if(aux2->ntyp == EVENTUALLY_P || aux2->ntyp == EVENTUALLY_PQ || 
				   aux2->ntyp == ALWAYS_P ||aux2->ntyp == ALWAYS_PQ||
				   aux2->ntyp == UNTIL_P || aux2->ntyp == UNTIL_PQ){
					Symbol *s = aux2->sym;
					while(s !=NULL){ 
						Symbol *p = s->params;
						int ix = 0;
						while(p!=NULL){	
																		
							if(strcmp(p->name, aux->sym->name)==0){							
								//fprintf(tl_out, "\t qVars[%s][%s] = %d;\n", p->name, s->name, ix);
								fprintf(tl_out, "\t qVars[%s][%s][%d] = 1;\n", p->name, s->name, ix);
							}
							ix++;							
							p = p->params;
						}
						s = s->next;
					}
					vId++;
				}
				

				aux2 = aux2->lft;
			}

		}
		vId = generate_initQVarsRec(aux->lft, vId);
		vId = generate_initQVarsRec(aux->rgt, vId);		
	} 
	return vId;
}
void generate_initQVars(Node *n){
	int qvars = generate_varIdsRec(n, 0);
	fprintf(tl_out,"#define QVARS %d\n", qvars);
	char *init = "inline qVarsInit(){ \n\
		c_code{ \n\
			for(int j = 0; j < QVARS; j++){ \n\	
				for(int i =0 ; i < EVENTS+1; i++){ \n\			
					for(int k= 0; k< COLS_E-2; k++)\n\
						qVars[j][i][k] = 0;\n\
				} \n\
			} \n";		
	char *end = "\t}\n}\n";
		

	fprintf(tl_out,"%s",init);	
	generate_initQVarsRec(n,0);
	fprintf(tl_out,"%s",end);	
}

int generate_variablesInitRec(Node *n, int fId){
	Node *aux = n;
	int nextfId = fId+1;

	switch(n->ntyp){
			case ALWAYS_P:
			case EVENTUALLY_P:
			case ALWAYS_PQ:			
			case EVENTUALLY_PQ:{
				Symbol *s = n->sym;
				int ix = 0;
				while(s!=NULL){
					Symbol *p = s->params;				
					int pos = 1;
					while(p!=NULL){																			
						fprintf(tl_out, "\t variables[%d][%d][%s] = %d;\n",fId , ix, p->name, pos);																				
						pos++;
						p = p->params;
					}
					ix++;
					s = s->next;
				}
				nextfId = generate_variablesInitRec(n->lft, nextfId);
				break;
			}				
			case UNTIL_P:
			case UNTIL_PQ:{
			Symbol *s = n->sym;
				int ix = 0;
				while(s!=NULL){
					Symbol *p = s->params;				
					int pos = 1;
					while(p!=NULL){																			
						fprintf(tl_out, "\t variables[%d][%d][%s] = %d;\n",fId , ix, p->name, pos);																				
						pos++;
						p = p->params;
					}
					ix++;
					s = s->next;
				}
				nextfId = generate_variablesInitRec(n->lft, nextfId);
				nextfId = generate_variablesInitRec(n->rgt,nextfId);
				break;
			}	
			case EXISTS:
			case FORALL:
			case NOT:				
				nextfId = generate_variablesInitRec(n->lft, nextfId);
				break;									
			case OR:
			case AND:{								
				nextfId = generate_variablesInitRec(n->lft, nextfId);
				nextfId = generate_variablesInitRec(n->rgt,nextfId);
				break;
			}
			case TRUE:				
			case PREDICATE:				
				break;
			default:
				printf("eLTL operator not implemented\n");
				alldone(1);
		}
	return nextfId;
}
void generate_variablesInit(Node *n){
	char *init = "inline variablesInit(){ \n\
	c_code{\n\
		for(int j = 0; j < FORMULAS; j++){\n\	
				for(int i =0 ; i < 2; i++){\n \				
					for(int k= 0; k< QVARS; k++)\n\
						variables[j][i][k] = 0;\n\
			}\n\
		}\n";		
	char *end = "\t}\n}\n";
	fprintf(tl_out,"%s",init);	
	generate_variablesInitRec(n,0);
	fprintf(tl_out,"%s",end);
}

void generate_promela(Node *n){	
	int formulas = 0;
	if(n){
		fprintf(tl_out,"\n");
		generate_initQVars(n);
		fprintf(tl_out,"\ninline myformula(){\n\t");	
		generate_run(n, 0, 1);
		fprintf(tl_out, "}\n");
		fprintf(tl_out,"\ninline runChild(f,c){\n\tif\n");	
		formulas = generate_runChild(n, 0);
		fprintf(tl_out, "\tfi;\n}\n"); 
		fprintf(tl_out, "#define FORMULAS %d\n", formulas);

		generate_variablesInit(n);
	}
	
}

void
tl_parse(void)
{       Node *n = tl_formula();
        if (tl_verbose)
	{	printf("formula: ");
		put_uform();
		printf("\n");
	}
	//trans(n); //LPJ: cambiar por una función que genere el fichero promela
	generate_promela(n);
}
