/* eltl2pml is freely distributed as part of STAN tool                    */
/* eltl2pml is an adaptation ltl2ba done by Laura Panizo, UMA, Malaga     */
/**************************************************************************/
/* Most of the code in this file was taken from ltl2ba                    */
#ifndef LTL2BA_UTIL_H
#define LTL2BA_UTIL_H

#include <sys/time.h>

int timeval_subtract (struct timeval *, struct timeval *, struct timeval *);

#endif
