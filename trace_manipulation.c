#include<stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include <math.h>
#include "trace_manipulation.h"


void readTraceCSV(char *filename, int cols, int rows, TTrace *traces){
	FILE *file = fopen(filename,"rt");
	int i=0, j=0, d,m,a,hh,mm;
	double ss;
	if(file==NULL) {

		perror("Error opening trace file");
		exit(-1);
	}
#ifdef VERB
	printf("File %s open\n", filename);
#endif
	traces->cols = cols;
	traces->rows = rows;
	traces->varTraces = (double **)malloc(sizeof(double *)*rows);
	for(i=0; i< rows; i++)
	{
		traces->varTraces[i] = (double *)malloc(sizeof(double)*cols);
	}
	for(i=0; i<rows; i++){
		for(j=0; j<cols; j++){
			if(j==0){
				//Wireshark UTC time format
				fscanf(file,"%d-%d-%d %d:%d:%lf",&a,&m,&d,&hh,&mm,&ss);
				traces->varTraces[i][j] = ss+(60*mm)+(hh*3600); //timestamp is expressed in seconds s
			
			}else{
				if(fscanf(file,"%lf",&(traces->varTraces[i][j]))==0){
#ifdef VERB
					printf("Error Reading row %d col %d \n", i,j);
#endif
					perror("Error reading file");
					exit(-2);
				}
			}
		}
	}

	fclose(file);
}

void destroyTrace(TTrace *traces)
{
	int i;
  if(traces->varTraces != NULL){
    for(i =0; i< traces->rows;i++)
    {
		  free(traces->varTraces[i]);
	 }
	 free(traces->varTraces);
	 traces->rows = 0;
	 traces->cols =0;
	 traces->varTraces = NULL;     
  }
#ifdef VERB
	printf("Trace destroyed\n");
#endif
}

void showTrace(TTrace traces){
  int i=0, c =0;
  while(i<traces.rows)
  {
    for(c=0; c<traces.cols; c++){
      printf("%lf ", traces.varTraces[i][c]);
    }
    printf("\n");
    i++;
  }
}


int nextEvent(TTrace traces, int t, int e)
{   int i = t;
    while(i>=0 && i< traces.rows && traces.varTraces[i][1]!=e) {i++;}
    
    if(i== traces.rows) i=-1;
//#ifdef VERB
//    printf("nextEvent e=%d from t=%d is at te=%d \n", e, t, i);
//#endif
    return i;
}

void printTime(double ss){
	int hh, mm;
	double r;
	hh = (int)ss / 3600;
	r = fmod(ss,3600);
	mm = (int)r / 60;
	r = fmod(r,60);
	printf("%d:%d:%lf\n", hh, mm, r);

}

#define TRUE_STRING "TRUE"
#define FALSE_STRING "FALSE"

void printResult(int pid, char * ptype, int result, int t){
#ifdef VERB
	printf("%s(%d) is %s at %d\n",ptype, pid, (result ?TRUE_STRING: FALSE_STRING), t);
#endif
}

void printEvent(int pid,char *ptype, int p, int tp){
#ifdef VERB
	printf("%s(%d) valid event at %d\n", ptype, pid, p, tp);
#endif	
}


void printInterval(int pid,char *ptype, int p, int tp, int q, int tq){
#ifdef VERB
	printf("%s(%d) valid interval of events at time interval [%d,%d]\n", ptype, pid, tp,tq);
#endif	
}