/* Events present in the system trace
 stt = 1 (start), stp = 2 (stop), fp = 3 (first picture), 
 l = 4 (low resolution), h = 5 (high resolution)
 */
mtype:event = {h,l,fp,stp,stt};
#define EVENTS 5 	//number of elements in the mtype:event

#define MEASURES_FILE "measures_0.txt"	//measures file name to be used
#define ROWS_M 134	//number of rows in the measures files
#define COLS_M 8	//number of columns in the measures file

#define EVENTS_FILE "events_0.txt"
#define ROWS_E 10	//number of rows in the event file
#define COLS_E 2	//number fo columns in the event file

#define FORMULAS 5 //number of eLTL operands in the formula

inline myformula(){
	/* (Eventually_[stt,stp] rx_data>14 MB ) AND Always_[h,l] avg_datarate<=1 MBps */
	run AND(0,1,2);	
	run EVENTUALLY_PQ(1,3,stt,stp);	
	run PHI_RXDATA(3,14);	
	run ALWAYS_PQ(2,4,h,l)
	run PHI_AVGRATE(4,1);
}



