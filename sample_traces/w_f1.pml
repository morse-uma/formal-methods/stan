/* Events present in the system trace
 syn = 2, rst = 4, ack = 10, 
 fin_ack = 11, syn_ack = 12, rst_ack=14, psh_ack=18, fin_psh_ack=19
 */
/*mtype:event = {syn, rst, ack, 
 fin_ack, syn_ack, rst_ack, psh_ack, fin_psh_ack};*/
#define EVENTS 8 	//number of elements in the mtype:event
#define syn 2
#define rst 4
#define ack 10
#define fin_ack 11
#define syn_ack 12
#define rst_ack 14
#define psh_ack 18
#define fin_psh_ack 19


#define MEASURES_FILE "w_measures.tsv"	//measures file name to be used
#define ROWS_M 33566	//number of rows in the measures files
#define COLS_M 8	//ts | srcPort | dstPort | iRTT | Length | Window | SeqNum | SeqAck 

#define EVENTS_FILE "w_events.tsv"
#define ROWS_E 33566	//number of rows in the event file
#define COLS_E 2	//number fo columns in the event file

#define FORMULAS 3 //number of eLTL operands in the formula

inline myformula(){
	/*Eventually [syn,fin_ack] TRUE  --> There is some connection that is closed with fin_ack event before other is stablished*/
	run EVENTUALLY_PQ(0,1,syn, syn); 
	run EVENTUALLY_PQ(1,2,syn,fin_ack);
    run PHI_TCP_PORTS(2);  
}




