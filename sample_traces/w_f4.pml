/* Events present in the system trace
 syn = 2, rst = 4, ack = 10, 
 fin_ack = 11, syn_ack = 12, rst_ack=14, psh_ack=18, fin_psh_ack=19
 */
/*mtype:event = {syn, rst, ack, 
 fin_ack, syn_ack, rst_ack, psh_ack, fin_psh_ack};*/
#define EVENTS 8 	//number of elements in the mtype:event
#define syn 2
#define rst 4
#define ack 10
#define fin_ack 11
#define syn_ack 12
#define rst_ack 14
#define psh_ack 18
#define fin_psh_ack 19


#define MEASURES_FILE "w_measures.tsv"	//measures file name to be used
#define ROWS_M 33566	//number of rows in the measures files
#define COLS_M 8	//ts | srcPort | dstPort | iRTT | Length | Window | SeqNum | SeqAck 


#define EVENTS_FILE "w_events.tsv"
#define ROWS_E 33566	//number of rows in the event file
#define COLS_E 2	//number fo columns in the event file

#define FORMULAS 8 //number of eLTL operands in the formula

inline myformula(){
	/* a reset in the connection between src and dst ports implies that at some point the iRRT was greater or equals to the threshold iRTT 
	 * */
	run OR(0,1,4)
	run NOT(1,2);
	run EVENTUALLY_PQ(2,3,syn,rst_ack);
	run PHI_CHECK_TCP_PORTS(3,56539, 443);
	
	run EVENTUALLY_PQ(4,5, syn,rst_ack);
	run AND(5,6,7);	
	run PHI_CHECK_TCP_PORTS(6,443, 56539);
	run PHI_iRTT(7,35);
}




