/* Events present in the system trace
 syn = 2, rst = 4, ack = 10, 
 fin_ack = 11, syn_ack = 12, rst_ack=14, psh_ack=18, fin_psh_ack=19
 */
/*mtype:event = {syn, rst, ack, 
 fin_ack, syn_ack, rst_ack, psh_ack, fin_psh_ack};*/
#define EVENTS 8 	//number of elements in the mtype:event
#define syn 2
#define rst 4
#define ack 10
#define fin_ack 11
#define syn_ack 12
#define rst_ack 14
#define psh_ack 18
#define fin_psh_ack 19


#define MEASURES_FILE "w_measures.tsv"	//measures file name to be used
#define ROWS_M 33566	//number of rows in the measures files
#define COLS_M 8	//ts | srcPort | dstPort | iRTT | Length | Window | SeqNum | SeqAck 


#define EVENTS_FILE "w_events.tsv"
#define ROWS_E 33566	//number of rows in the event file
#define COLS_E 2	//number fo columns in the event file

#define FORMULAS 9 //number of eLTL operands in the formula

inline myformula(){
	/*If a connection is open between src and dst ports, it must be closed at some time instant
	* (Eventually_[syn, syn_ack] PHI_CHECK_TCP_PORTS(src,dst)) -> ((Eventually_[fin_ack]PHI_CHECK_TCP_PORTS(src,dst)|| Eventually_[fin_psh_ack]PHI_CHECK_TCP_PORTS(src,dst)))
	* (NOT (Eventually_[syn, syn_ack] PHI_CHECK_TCP_PORTS(src,dst))) OR (Eventually_[fin]PHI_CHECK_TCP_PORTS(src,dst)) */
	run OR(0,1,4)
	run NOT(1,2);
	run EVENTUALLY_PQ(2,3,syn,ack);
	run PHI_CHECK_TCP_PORTS(3,56539, 443);
	
	run OR(4,5,7);
	run EVENTUALLY_P(5,6, fin_ack);
	run PHI_CHECK_TCP_PORTS(6,443, 56539);
	run EVENTUALLY_P(7,8, fin_psh_ack);
	run PHI_CHECK_TCP_PORTS(8,443, 56539);
}




