/* TCP ports are the same in the interval end points although they can be swap
 *	This implementation asumes that the event and measures trace files have the same length
 */
proctype PHI_TCP_PORTS(int id){
	int ti, tf;
	bool phi;
end_init_phi:cm[id]?ti,START;
	cm[id]?tf,STOP; 	
	c_code{ 
  		  PPHI_TCP_PORTS->phi = ((measures.varTraces[PPHI_TCP_PORTS->ti][1] == measures.varTraces[PPHI_TCP_PORTS->tf][1])
  		  			&& (measures.varTraces[PPHI_TCP_PORTS->ti][2] == measures.varTraces[PPHI_TCP_PORTS->tf][2]))||
  		  			((measures.varTraces[PPHI_TCP_PORTS->ti][1] == measures.varTraces[PPHI_TCP_PORTS->tf][2])
  		  			&& (measures.varTraces[PPHI_TCP_PORTS->ti][2] == measures.varTraces[PPHI_TCP_PORTS->tf][1]));					
					printf("PHI_TCP_PORTS interval [%d, %d]result %d\n", PPHI_TCP_PORTS->ti, PPHI_TCP_PORTS->tf, PPHI_TCP_PORTS->phi);
		};

stopped_phi:
	rd[id]!phi,tf;
	goto end_init_phi;	

}


/*  Check if src and dst TCP ports are the expected in a specific time intant
 *	This implementation asumes that the event and measures trace files have the same length
 */
proctype PHI_CHECK_TCP_PORTS(int id;int src; int dst){
	int ti,tf;
	bool phi;
end_init_phi:cm[id]?ti,START;
	cm[id]?tf,STOP; 
	c_code{ 
  		  PPHI_CHECK_TCP_PORTS->phi = (measures.varTraces[PPHI_CHECK_TCP_PORTS->tf][1] == PPHI_CHECK_TCP_PORTS->src)
  		  			&& (measures.varTraces[PPHI_CHECK_TCP_PORTS->tf][2] == PPHI_CHECK_TCP_PORTS->dst);					
					//printf("PPHI_CHECK_TCP_PORTS id= %d at t2=%d result %d\n", PPHI_CHECK_TCP_PORTS->id,PPHI_CHECK_TCP_PORTS->tf,PPHI_CHECK_TCP_PORTS->phi);
		};

stopped_phi:
	rd[id]!phi,tf;
	goto end_init_phi;	

}

/* Determines if the iRTT is below the given threshold during a time interval
 *	This implementation asumes that the event and measures trace files have the same length 
 */
 proctype PHI_iRTT_BELOW (int id; int microseg){
 int ti,tf;
 bool phi = 1;
 end_init_phi: cm[id]?ti,START;
 	cm[id]?ti,STOP;
 	c_code{
 		int i = PPHI_iRTT_BELOW->ti;

 		while(i <= PPHI_iRTT_BELOW->tf && measures.varTraces[i][3]*1000 < PPHI_iRTT_BELOW->microseg){
 			i++;
 		}
 		PPHI_iRTT_BELOW->phi = i>PPHI_iRTT_BELOW->tf ? 1: 0;
 	}; 
stopped_phi:
	rd[id]!phi,tf;
	goto end_init_phi;
 }
 
/* Determines if the iRTT is greater than the given threshold at some point in time interval
 *	This implementation asumes that the event and measures trace files have the same length 
 */
 proctype PHI_iRTT (int id; int microseg){
 int ti,tf;
 bool phi = 1;
 end_init_phi: cm[id]?ti,START;
 	cm[id]?ti,STOP;
 	c_code{
 		int i = PPHI_iRTT->ti;

 		while((i <= PPHI_iRTT->tf) && (measures.varTraces[i][3]*1000 < PPHI_iRTT->microseg)){
 			i++;
 		}
 		PPHI_iRTT->phi = i>PPHI_iRTT->tf ? 0: 1;
 	}; 
stopped_phi:
	rd[id]!phi,tf;
	goto end_init_phi;
 }