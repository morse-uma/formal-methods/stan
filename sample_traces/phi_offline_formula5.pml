/***************************************************************************************/
proctype PHI_RXDATA(int id; int value)
{
	int ti, te;
	bool phi;
end_init_phi:cm[id]?ti,te; 	
	
  c_code{ int t1 = events.varTraces[PPHI_RXDATA->ti][0];
  		  int t2 = events.varTraces[PPHI_RXDATA->te][0];
  		  int i =0;
  		  float rx_data = 0;
  		  while(i < measures.rows && measures.varTraces[i][0]< t1) i++;
  		  if (i==measures.rows) PPHI_RXDATA->phi = 0; /*esto no debería hacer falta*/
  		  else{
  		    
  		    while(i < measures.rows && measures.varTraces[i][0]< t2)
  		    {
  		    	rx_data += measures.varTraces[i][4];
  		    	i++;
  		    }
  		  	PPHI_RXDATA->phi = rx_data >= (PPHI_RXDATA->value)?1:0; 
  		  }        
         };
stopped_phi:
	rd[id]!phi,te;
	goto end_init_phi;
}

/***************************************************************************************/
proctype PHI_AVGRATE(int id; int value)
{
	int ti, te;
	bool phi;
end_init_phi:cm[id]?ti,te; 	
	
  c_code{ int t1 = events.varTraces[PPHI_AVGRATE->ti][0];
  		  int t2 = events.varTraces[PPHI_AVGRATE->te][0];
  		  int i =0;
  		  float avg_rate = 0;
  		  while(i < measures.rows && measures.varTraces[i][0]< t1) i++;
  		  if (i==measures.rows) PPHI_AVGRATE->phi = 0;
  		  else{
  		    
  		    while(i < measures.rows && measures.varTraces[i][0]< t2)
  		    {
  		    	avg_rate += measures.varTraces[i][5];
  		    	i++;
  		    }
  		  	PPHI_AVGRATE->phi = avg_rate/(t2-t1) <= (PPHI_AVGRATE->value)?1:0; 
  		  }        
         };
stopped_phi:
	rd[id]!phi,te;
	goto end_init_phi;
}

/**************************************************************************************/

