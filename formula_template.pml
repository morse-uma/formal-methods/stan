// mtype::event is a enum whose values represent in the system trace
// enum values are assigned from highest to lowest, bein 1 the minimum value. 
// In the example stt ==1 and h ==5
mtype:event = {h,l,fp,stp,stt};

#define EVENTS 5 	//number of elements in the mtype:event

#define MEASURES_FILE "measures_0.txt"	//file with measures
#define ROWS_M 134	//number of rows of MEASURES_FILE. Each row has a different timestamp
#define COLS_M 8	//number of columns of MEASURES_FILES. The first column is the timestamp, the rest are real-valued/integer magnitudes 

#define EVENTS_FILE "events_0.txt" //file with events
#define ROWS_E 10	//number of rows of EVENTS_FILE. Each row has a different timestamp
#define COLS_E 2	//number fo columns of EVENTS_FILE. The first column is the time stamp, the second is the event number

#define FORMULAS 2 //number of eLTL operands in the formula
inline myformula(){
	//Place here the instantiation of the monitors
	run EVENTUALLY_PQ(0,1,stt,stp);
    run TRUE(1);  
}





