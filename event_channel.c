#include<stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <time.h>
#include "event_channel.h"

ChannelEv *createEmptyChannel()
{	
	ChannelEv *ch = (ChannelEv *)malloc(sizeof(ChannelEv));
	ch->h = (P_Event)malloc(sizeof(struct Event));
	ch->h->t = -1;
	ch->h->e = -1; 
	ch->h->next = NULL;
	ch->t = ch->h;
	return ch;
}
int isEmpty(ChannelEv ch)
{
	return !(ch.h->next);
}
void destroyChannel(ChannelEv *ch)
{
	P_Event aux = NULL;
	while(ch->h!=NULL){
		aux = ch->h;
		ch->h = ch->h->next;
		free(aux);
	}
	free(ch);
	ch = NULL;
}
void showChannel(ChannelEv ch)
{
	P_Event aux = ch.h->next;
	if(!aux)
		printf("Empty channel\n");
	else{
		printf("channel: \t");
		while(aux)
		{	
			printf("t: %d e:%d\t", aux->t, aux->e);			
			aux = aux->next;
		}
	}
}

void showProctypeChannel(ChannelEv ch, P_Event p)
{
	
	P_Event aux = ch.h->next;
	if(!aux)
		printf("Empty channel\n");
	else{
		printf("channel: \t");
		if(ch.h == p)
			printf("non-processed events: ");
		while(aux)
		{
			if(p==aux)
				printf("non-processed events: ");
			printf("t: %d e:%d\t", aux->t, aux->e);
			aux = aux->next;		
		}
	}
	printf("\n");
}

int insertEvent(ChannelEv *ch, P_Event *p, int f, int t, int e )
{ int i;
	ch->t->next = (P_Event)malloc(sizeof(struct Event));
	ch->t = ch->t->next;
	ch->t->t = t;
	ch->t->e = e;
	ch->t->next = NULL;	
	for(i = 0; i < f; i++){
		if(p[i] == NULL)
			p[i]= ch->t;
	}
	return 1;
}

int processEvent(ChannelEv ch, P_Event *p, int *t, unsigned char *e)
{
	if(*p == NULL || ch.h->next == NULL){
		return 0;		
	}
	else if(*p==ch.h){
		*t = ch.h->next->t;
		*e = ch.h->next->e;
		*p = ch.h->next->next;
	}
	else{		
		*t = (*p)->t;
		*e = (*p)->e;
		(*p) = (*p)->next;
	}	
	return 1;
}


void backtrackTo(ChannelEv ch, P_Event *p, int t)
{	
	*p = ch.h->next;		
	while((*p)!=NULL && (*p)->t< t){ 		
		(*p) = (*p)->next;		
	}	
}