#ifndef EVENT_CHANNEL_H_

#define EVENT_CHANNEL_H_


struct Event{
  int t;  //timestamp
  unsigned char e;  //event id
  struct Event *next; 
};

typedef struct Event *P_Event;
 
typedef struct ChannelEv{
	P_Event h; //head
	P_Event t; //tail
}ChannelEv;

ChannelEv *createEmptyChannel();
int isEmpty(ChannelEv ch);
int insertEvent(ChannelEv *ch, P_Event *p, int f, int t, int e );
void destroyChannel(ChannelEv *ch);
void showChannel(ChannelEv ch);
void showProctypeChannel(ChannelEv ch, P_Event p);
int processEvent(ChannelEv ch, P_Event *p,int *t, unsigned char *e);
void backtrackTo(ChannelEv ch, P_Event *p, int t);

#endif
