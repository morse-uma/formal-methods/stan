#ifndef TRACE_MANIPULATION_H_

#define TRACE_MANIPULATION_H_

typedef struct {
  int cols;
  int rows;
  double **varTraces;
}TTrace;


void readTraceCSV(char *filename, int cols, int rows, TTrace *traces);

void destroyTrace(TTrace *traces);

void showTrace(TTrace traces);

int nextEvent(TTrace traces, int t, int e);

void printTime(double ss);

void printResult(int pid, char * ptype, int result, int t);

void printEvent(int pid,char *ptype, int p, int tp);

void printInterval(int pid,char *ptype, int p, int tp, int q, int tq);

#endif
