c_decl{
#include "trace_manipulation.h"
#include "event_channel.h"
TTrace events,measures;
ChannelEv *ev;
P_Event proc[FORMULAS];
}

mtype:command = {START, STOP};
chan rd[FORMULAS] = [0] of {bool, int}; //result,t
chan cm[FORMULAS] = [0] of {int,mtype:command}; //t,CM



/***************************************************************************************/
proctype UNTIL_PQ(int id; int c1; int c2; int P; int Q){
	int ti,tf,t,tp,tq, tc1, tc2;	
	bool stop, result, resultC1;
	mtype:event e;

init_until:
	stop = 0; 
end_until:
	cm[id]?ti,START;  cm[c1]!ti,START;
waitP:  
	   if
	   :: c_expr{ev->h->next && proc[PUNTIL_PQ->id]} -> 
	       
	        c_code{ processEvent(*ev, &proc[PUNTIL_PQ->id],&(PUNTIL_PQ->t), &(PUNTIL_PQ->e));}
	        
	        if
	        :: e == P && ti<=t && !stop -> tp = t;
	        		cm[c1]!tp,STOP; rd[c1]? resultC1;	        		
	        		cm[c2]!tp,START; goto waitQ;  
	        :: e == P && ti<=t && (stop && t<tf) -> tp = t;	        		
	        		cm[c1]!tp,STOP; rd[c1]? resultC1;	        	
	        		cm[c2]!tp,START; goto waitQ;
	        :: (!stop && (e!=P || t<ti)) -> goto waitP;
	        :: (stop && e!=P && t<=tf) -> goto waitP;
            :: stop && tf<t -> goto waitC1F;
	        fi;
       :: stop && c_expr{!(ev->h->next && proc[PUNTIL_PQ->id])} ->       		 
       		goto waitC1F;
	   :: cm[id]?tf,STOP; stop = 1; goto waitP;
	   fi;
	
waitQ:  
	   if
	   :: c_expr{ev->h->next && proc[PUNTIL_PQ->id]} -> 
	        c_code{ processEvent(*ev, &proc[PUNTIL_PQ->id],&(PUNTIL_PQ->t), &(PUNTIL_PQ->e));}
	        if
	        :: e == Q && (!stop || (e == Q && stop && t<tf)) -> atomic{
	        		tq = t;
					c_code{printInterval(PUNTIL_PQ->id, "UNTIL_PQ", PUNTIL_PQ->P, PUNTIL_PQ->tp, PUNTIL_PQ->Q, PUNTIL_PQ->tq);};
	        		};
	        		goto waitC2; 	         
	        :: (!stop && e!=Q) -> goto waitQ;
	        :: (stop && e!=Q && t<=tf) -> goto waitQ;
            :: stop && tf<t -> 
            		c_code{backtrackTo(*ev, &proc[PUNTIL_PQ->id], PUNTIL_PQ->tf);}; 
            		goto waitC2F;
	        fi;
       :: stop && c_expr{!(ev->h->next && proc[PUNTIL_PQ->id])} -> 
       		goto waitC2F;
	   :: cm[id]?tf,STOP; stop = 1; goto waitQ;
	   fi;

waitC2F: 
	cm[c2]!tf,STOP;
	rd[c2]?_; goto waitC1F;
waitC2: 
	cm[c2]!tq,STOP;
	rd[c2]?result, tc2;	
	if
	:: !result && !stop -> 	c_code{backtrackTo(*ev, &proc[PUNTIL_PQ->c1], PUNTIL_PQ->ti);
								   backtrackTo(*ev, &proc[PUNTIL_PQ->id], PUNTIL_PQ->tq);};							
					cm[c1]!ti,START;
					goto waitP;  
	:: !stop && result -> result = resultC1; goto wait_stop;
	:: else -> result = result && resultC1; goto until_pq;  
	fi;

waitC1F: 
	cm[c1]!tf,STOP; 
	rd[c1]?_, tc1; result= false; t =tq; goto until_pq;

wait_stop: 
	cm[id]?tf,STOP;
	if
	:: tq > tf -> result = false; 
	:: else -> t = tq; 
	fi;

until_pq: 
	if
	:: t >= tf -> c_code{backtrackTo(*ev, &proc[PUNTIL_PQ->id], PUNTIL_PQ->tf);};
	:: else -> t = tq; 
	fi;
	rd[id]!result,t;
	c_code{printResult(PUNTIL_PQ->id, "UNTIL_PQ", PUNTIL_PQ->result, PUNTIL_PQ->t);}; 
	goto init_until;
}


/*****************************************************/
proctype UNTIL_P(int id; int c1; int c2; int P){
	int ti,tf,t,tp, tc1, tc2;	
	bool stop, result, resultC1, c1result, c2result, c1stop, c2stop;
	mtype:event e;

init_until:
	stop = 0;
end_until: 
	cm[id]?ti,START;  cm[c1]!ti,START; 
	c1result =0; c2result=0; c1stop=0; c2stop=0;
waitP:  
	if
	:: c_expr{ev->h->next && proc[PUNTIL_P->id]} -> 
	    c_code{ processEvent(*ev, &proc[PUNTIL_P->id],&(PUNTIL_P->t), &(PUNTIL_P->e));}
	    if
	    :: e == P && ti<=t && (!stop || (stop && t<tf))-> atomic{
	    			tp = t;
					c_code{printEvent(PUNTIL_P->id, "UNTIL_P", PUNTIL_P->P, PUNTIL_P->tp);}; 
	    			 };
	    			goto waitC2; 	   
	    :: (!stop && (e!=P || t<ti)) -> goto waitP;
	    :: (stop && e!=P && t<=tf) -> goto waitP;
        :: stop && tf<t -> goto waitC1F;
		fi;
    :: stop && c_expr{!(ev->h->next && proc[PUNTIL_P->id])} -> 
    		goto waitC1F;
   :: cm[id]?tf,STOP; stop = 1; goto waitP;
   fi;
	
waitC2: 
	cm[c2]!tp,START;
	do 
	:: rd[c2]? result,t; c2result =1;
	:: cm[c2]!tp,STOP; c2stop = 1;
	::c2stop && c2result -> break;
	od;
	
	if
	:: !result && !stop -> goto waitP; 
	:: else ->  goto waitC1; 
	fi;

waitC1F: 
	do 
	:: rd[c1]? _,t; c1result =1;
	:: cm[c1]!tp,STOP; c1stop = 1;
	::c1stop && c1result -> break;
	od;
	result= false; goto until_p;

waitC1:
	do 
	:: rd[c1]? resultC1,t; c1result =1;
	:: cm[c1]!tp,STOP; c1stop = 1;
	:: c1stop && c1result -> break;
	od;
	result = result && resultC1; 
	rd[id]!result,tp;
	c_code{printResult(PUNTIL_P->id, "UNTIL_P", PUNTIL_P->result, PUNTIL_P->tp);}; 
	if
	:: stop -> goto until_p;
	:: else -> skip;
	fi;

wait_stop: 
cm[id]?tf,STOP;
	result = (tp > tf -> false: result)

until_p:  
	    if
		:: t >= tf -> c_code{backtrackTo(*ev, &proc[PUNTIL_P->id], PUNTIL_P->tf);}; 
		:: else; 
		fi;
	    goto init_until;

}
/************************ EVENTUALLY [P,Q]******************************************/
proctype EVENTUALLY_PQ(int id; int c1; int P; int Q){
	int ti,tf,t,tp,tq;	
	bool stop, result, sr, c1result, c1stop;
	mtype:event e;

init_eventually_pq: 
	stop = 0; result = false;
end_eventually: cm[id]?ti,START;
waitP: c1stop=0; c1result= 0;  		
	   if
	   :: c_expr{ev->h->next && proc[PEVENTUALLY_PQ->id]} -> 
	        
	        c_code{processEvent(*ev, &proc[PEVENTUALLY_PQ->id],&(PEVENTUALLY_PQ->t), &(PEVENTUALLY_PQ->e));};
	        if
	        :: e == P && ti<=t && (!stop || (stop && t<tf) )-> tp = t;
	        				cm[c1]!tp,START; 
	        				goto waitQ; 
	        :: (!stop && (e!=P || t<ti)) || (stop && e!=P && t<=tf) -> goto waitP;
            :: stop && tf<t -> t = tf; goto eventually_pq;
	        fi;
       :: stop && c_expr{!(ev->h->next && proc[PEVENTUALLY_PQ->id])} -> t = tf; goto eventually_pq;
	   :: cm[id]?tf,STOP; stop = 1;
	   			goto waitP;
	   fi;

waitQ: 
	   if
	   :: c_expr{ev->h->next && proc[PEVENTUALLY_PQ->id]} -> 	   		
	        c_code{processEvent(*ev, &proc[PEVENTUALLY_PQ->id],&(PEVENTUALLY_PQ->t), &(PEVENTUALLY_PQ->e));};
	        	
	        if
	        :: e == Q && ((!stop)|| (stop && t<tf))-> atomic{tq = t;
	        		c_code{printInterval(PEVENTUALLY_PQ->id, "EVENTUALLY_PQ", PEVENTUALLY_PQ->P, PEVENTUALLY_PQ->tp, PEVENTUALLY_PQ->Q, PEVENTUALLY_PQ->tq);}; 
	        		 };
	        		goto waitC1;  	        
	        :: e!=Q &&((!stop) || (stop && t<=tf))  -> goto waitQ;
            :: stop && tf<t -> t = tf; goto waitC1F;
	        fi;
       :: stop && c_expr{!(ev->h->next && proc[PEVENTUALLY_PQ->id])} ->        			
       			goto waitC1F;
	   :: cm[id]?tf,STOP; stop = 1; t = tf;	
	   			goto waitQ;	   			   
	   fi;

waitC1F:
	do  
	:: rd[c1]?_,t -> c1result= 1; 
	:: cm[c1]!t,STOP -> c1stop =1;
	:: c1stop && c1result -> t =tf; goto eventually_pq;
	od;

waitC1:
	do  
	:: rd[c1]?result,t -> c1result= 1; 
	:: cm[c1]!tq,STOP -> c1stop =1; 
	:: c1stop && c1result -> t = tq; break;
	od;
			
	if
	:: !result && !stop -> c_code{	backtrackTo(*ev, &proc[PEVENTUALLY_PQ->id], PEVENTUALLY_PQ->tq);};
							goto waitP; 
	:: else;
	fi;

eventually_pq:
	rd[id]!result,t;
	c_code{printResult(PEVENTUALLY_PQ->id, "EVENTUALLY_PQ", PEVENTUALLY_PQ->result, PEVENTUALLY_PQ->t);}; 
	if
	::stop; 
	::else -> cm[id]?tf,STOP; 		
	fi;
	
	if
	:: t >= tf -> c_code{backtrackTo(*ev, &proc[PEVENTUALLY_PQ->id], PEVENTUALLY_PQ->tf);}; 
	:: else ; 
	fi;
	goto init_eventually_pq;
}

/******************* EVENTUALLY [P] *******************************************/
proctype EVENTUALLY_P(int id; int c1; int P){
	int ti,tf,t,tp;	
	bool stop, result,c1stop, c1result;
	mtype:event e;

init_eventually_p: 
	stop = 0; result = false;
end_eventually: cm[id]?ti,START; 
waitP: 
	c1result = 0; c1stop = 0;
	if
	:: c_expr{ev->h->next && proc[PEVENTUALLY_P->id]} -> 	   		
	      c_code{processEvent(*ev, &proc[PEVENTUALLY_P->id],&(PEVENTUALLY_P->t), &(PEVENTUALLY_P->e));};
	      if
          :: e == P && ti<=t &&( !stop || (stop &&  t<tf)) -> atomic{ tp = t; 
          			c_code{printEvent(PEVENTUALLY_P->id, "EVENTUALLY_P", PEVENTUALLY_P->P, PEVENTUALLY_P->tp);}; 
          			};
          			goto waitC1;  
          :: (!stop && (e!=P || t<ti)) || (stop && e!=P && t<=tf) -> goto waitP;            
          :: stop && tf<t -> goto eventually_p;
          fi;
	:: stop && c_expr{!(ev->h->next && proc[PEVENTUALLY_P->id])} -> goto eventually_p;
	:: cm[id]?tf,STOP; stop = 1; t = tf; goto waitP;
	fi;

waitC1: cm[c1]!tp,START;
	do 
	:: rd[c1]? result,t; c1result =1;
	:: cm[c1]!tp,STOP; c1stop = 1;
	:: c1stop && c1result -> t =tp; break;
	od;

	if
	:: !stop && !result ->goto waitP; 
	:: else;
	fi;

eventually_p:  
	rd[id]!result,t; 
	c_code{printResult(PEVENTUALLY_P->id, "EVENTUALLY_P", PEVENTUALLY_P->result, PEVENTUALLY_P->t);};   
	if
	:: stop;
	:: else -> cm[id]?tf,STOP		
	fi;
	if
	:: t >= tf -> c_code{backtrackTo(*ev, &proc[PEVENTUALLY_P->id], PEVENTUALLY_P->tf);}; 			
	:: else;
	fi;
	goto init_eventually_p;
}


/******************* ALWAYS [P] *******************************************/
proctype ALWAYS_P(int id; int c1; int P){
	int ti,tf,t,tp;	
	bool stop, result, c1stop, c1result;
	mtype:event e;
	 
init_always_p:
	stop = 0; result = true;
end_always_p: cm[id]?ti,START; 
waitP:
	c1stop=0; c1result =0;
	if
	:: c_expr{ev->h->next && proc[PALWAYS_P->id]} -> 	   		
	      c_code{processEvent(*ev, &proc[PALWAYS_P->id],&(PALWAYS_P->t), &(PALWAYS_P->e));};
	        if
	        :: e == P && ti<=t && (!stop || (stop && t<tf))-> atomic{tp = t;
				c_code{printEvent(PALWAYS_P->id, "ALWAYS_P", PALWAYS_P->P, PALWAYS_P->tp);}; 
	        	};  
	        	goto waitC1;  
	        :: (!stop && (e!=P || t<ti)) || (stop && e!=P && t<=tf) -> goto waitP;
            :: stop && tf<t -> goto always_p;
	        fi;
	:: stop && c_expr{!(ev->h->next && proc[PALWAYS_P->id])} -> goto always_p;
	:: cm[id]?tf,STOP; stop = 1; t = tf; goto waitP;
	fi;

waitC1: cm[c1]!tp,START; 
	do 
	:: rd[c1]? result,t; c1result =1;
	:: cm[c1]!tp,STOP; c1stop = 1;
	:: c1stop && c1result -> t=tp; break;
	od;
	if
	:: !stop && result -> goto waitP; 
	:: else; 
	fi;

always_p:
	rd[id]!result,t;
	c_code{printResult(PALWAYS_P->id, "ALWAYS_P", PALWAYS_P->result, PALWAYS_P->t);};	
	if
	:: stop;
	:: else -> cm[id]?tf,STOP -> 		
	fi;
	if
	:: t >= tf -> c_code{backtrackTo(*ev, &proc[PALWAYS_P->id], PALWAYS_P->tf);}; 
	:: else; 
	fi;
	goto init_always_p;

}

/************************ ALWAYS [P,Q]******************************************/
proctype ALWAYS_PQ(int id; int c1; int P; int Q){
	int ti,tf,t,tp,tq;	
	bool stop, result, c1result, c1stop;
	mtype:event e;

init_always_pq:
	stop = 0;	result = 1;
end_always: cm[id]?ti,START; 
waitP: c1result =0; c1stop=0;
	   if
	   :: c_expr{ev->h->next && proc[PALWAYS_PQ->id]} -> 	   		
	      c_code{processEvent(*ev, &proc[PALWAYS_PQ->id],&(PALWAYS_PQ->t), &(PALWAYS_PQ->e));	      	
	      };
	        if
	        :: e == P && ti<=t && (!stop || (stop && t<tf)) -> tp = t; cm[c1]!tp,START; 		             									        								
	        								goto waitQ;  
	        :: (!stop && (e!=P || t<ti)) || (stop && e!=P && t<=tf) -> goto waitP;
            :: stop && tf<t -> t = tf; goto always_pq;
	        fi;       
	   :: cm[id]?tf,STOP; stop = 1; goto waitP;
	   :: stop && c_expr{!(ev->h->next && proc[PALWAYS_PQ->id])} -> 	   							
	   						t = tf; goto always_pq;
	   fi;

waitQ: 
	   if
	   :: c_expr{ev->h->next && proc[PALWAYS_PQ->id]} -> 	   		
	      c_code{processEvent(*ev, &proc[PALWAYS_PQ->id],&(PALWAYS_PQ->t), &(PALWAYS_PQ->e));	       
	      };
	     
	        if
	        :: e == Q && ((!stop)|| (stop && t<tf))  -> atomic{ tq = t;
					c_code{printInterval(PALWAYS_PQ->id, "ALWAYS_PQ", PALWAYS_PQ->P, PALWAYS_PQ->tp, PALWAYS_PQ->Q, PALWAYS_PQ->tq);}; 
	        		};
	        	goto waitC1;  
	        :: e!=Q &&((!stop)||(stop && t<=tf)) -> goto waitQ;
            :: stop && tf<t -> goto waitC1T;
	        fi;
       :: stop && c_expr{!(ev->h->next && proc[PALWAYS_PQ->id])} -> goto waitC1T;
	   :: cm[id]?tf,STOP; stop = 1;  
	   					  goto waitQ;
	   fi;

waitC1T:
	do  
	:: rd[c1]?_,t -> c1result= 1; 
	:: cm[c1]!t,STOP -> c1stop =1; 
	:: c1stop && c1result -> t =tf; break;
	od;
	goto always_pq;

waitC1: 
	do  
	:: rd[c1]?result,t -> c1result= 1; 
	:: cm[c1]!tq,STOP -> c1stop =1;  
	:: c1stop && c1result -> t = tq; break;
	od;	
	if
	:: result && !stop -> c_code{backtrackTo(*ev, &proc[PALWAYS_PQ->id], PALWAYS_PQ->tq);};  goto waitP; 
	:: else;
	fi;

always_pq: 
	rd[id]!result,t; 
	c_code{printResult(PALWAYS_PQ->id, "ALWAYS_PQ", PALWAYS_PQ->result, PALWAYS_PQ->t);};
	if
	::stop; 
	::else -> cm[id]?tf,STOP;		
	fi;    
	if
	:: t >= tf -> c_code{backtrackTo(*ev, &proc[PALWAYS_PQ->id], PALWAYS_PQ->tf);}; 
	:: else; 
	fi;
	goto init_always_pq;

}

/***************************************************************************************/
proctype OR(int id; int c1; int c2){
	int ti, tf, tc1, tc2,t;
	bool phi, phiC1, phiC2, readyC1, readyC2, sr, st;
init_or: 
    readyC1=0; readyC2=0; sr=0; st = 0; 
end_or:
    cm[id]?ti,START; cm[c1]!ti,START; cm[c2]!ti,START;
started_or:
	 
	do
	:: cm[id]?tf,STOP -> st = 1; 
		cm[c1]!tf,STOP; cm[c2]!tf,STOP; 
	:: rd[c1]?phiC1,tc1 -> readyC1 = 1; 
		if
		:: phiC1 && !sr -> sr = 1; rd[id]!phiC1,tc1; 
		:: else;
		fi;
	:: rd[c2]?phiC2,tc2 -> readyC2 = 1;
		if
		:: phiC2 && !sr -> sr = 1; rd[id]! phiC2,tc2;
		:: else;
		fi; 
	:: !sr && readyC1 && readyC2 -> atomic{
		t = (tc1<tc2 -> tc2 : tc1);	
		phi = (phiC1 || phiC2);			
		c_code{printResult(POR->id, "OR", POR->phi, POR->t);};
		sr = 1;};
		rd[id]!phi, t; 
	:: sr && st -> break;
	od;
	goto init_or;
}

/***************************************************************************************/
proctype AND(int id; int c1; int c2)
{
	int ti, tf, t, tc1,tc2;
	bool phi, phiC1, phiC2, readyC1, readyC2, sr, st;
init_and: 
    readyC1=0; readyC2=0; sr=0; st = 0;
end_and:
    cm[id]?ti,START; cm[c1]!ti,START; cm[c2]!ti,START;

started_and:	
	do
	:: cm[id]?tf,STOP -> cm[c1]!tf,STOP; cm[c2]!tf,STOP; 
		if
		:: sr -> break;
		:: else-> st =1;
		fi;
	:: rd[c1]?phiC1,tc1 -> readyC1 = 1; 
		if
		:: !phiC1 && !sr -> rd[id]! phiC1,tc1; sr = 1; 
		:: else;
		fi;
	:: rd[c2]?phiC2,tc2 -> readyC2 = 1; 
		if
		:: !phiC2 && !sr -> rd[id]! phiC2,tc2; sr = 1;
		:: else;
		fi;	
	:: !sr && readyC1 && readyC2 -> atomic{
		t = (tc1<tc2 -> tc2 : tc1);
		phi = (phiC1 && phiC2);
		c_code{printResult(PAND->id, "AND", PAND->phi, PAND->t);}; 
		sr =1;};		
		rd[id]!phi, t;		
	:: sr && st -> break;
	od;

	goto init_and;
	
}
/***************************************************************************************/
proctype NOT(int id; int c1){
	int ti, tf,t;
	bool phiC1, sr, st;
 
end_not:	cm[id]?ti,START; 
	cm[c1]!ti,START;
	sr =  0; st = 0;	
	do
	:: cm[id]?tf,STOP -> cm[c1]!tf,STOP; st = 1;			
	:: rd[c1]?phiC1,t -> atomic{c_code{printResult(PNOT->id, "NOT", !(PNOT->phiC1), PNOT->t);};
								sr = 1;};
						rd[id]!(!phiC1),t; 
	:: sr && st -> break;
	od;

	goto end_not;
	
}
/***************************************************************************************/
proctype TRUE(int id){
	int ti, tf;
	bool result = 1;

end_init_true: cm[id]?ti,START; 

stopped_true:
	rd[id]!result,ti; 
	c_code{printResult(PTRUE->id, "TRUE", 1, PTRUE->tf);};
	cm[id]?tf,STOP;
	goto end_init_true;
}


/**************************************************************************************/
int i;
mtype:event e;
inline sendEvent(){	
	c_code{ 
#ifdef VERB
	printf("sendEvent e:%d i:%d\n", now.e,now.i);
#endif
			insertEvent(ev,proc, FORMULAS, now.i,now.e);
	};

}
/***************************************************************************************/

active proctype formula(){
	bool result,rx=0, stop = 0;
	int t;
	c_code{		
		int i;
		readTraceCSV(MEASURES_FILE, COLS_M, ROWS_M, &measures);
		readTraceCSV(EVENTS_FILE, COLS_E, ROWS_E, &events);
		ev = createEmptyChannel();
		for(i=0; i<FORMULAS; i++)			
			proc[i]= ev->h;
	}
	atomic{myformula();};
	do
	:: stop && rx -> break;
	:: !(stop && rx) && i>=0 && i<ROWS_E -> c_code{now.e = events.varTraces[now.i][1];};
			if			
			:: i == 0 -> cm[0]!i,START; sendEvent();
			:: i == ROWS_E -1 -> sendEvent(); cm[0]!i,STOP;	stop =1;			
			:: else -> sendEvent();			
			fi;
			i++;
	:: rd[0]?result,t; 
		if
		:: !stop -> cm[0]!i,STOP; stop=1;	
		:: else;
		fi;	
		rx =1;		

	od;
	
init_stop: 
 	if	
    ::result -> c_code{printf("Property SATISFIED at ");};	
    ::else ->  c_code{printf("Property NOT SATISFIED at "); };					 
    fi;
c_code{	printTime(events.varTraces[Pformula->t][0]);
		destroyTrace(&measures); destroyTrace(&events); destroyChannel(ev);
	  }
assert(false); 
accept_formula:
}
