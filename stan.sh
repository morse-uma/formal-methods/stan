#!/bin/bash 

MODE="offline"
DEBUG=0
PHI_SET=0
PHI=""
FORM_SET=0

while getopts 'nf:a:d' flag; do
	case "${flag}" in 
		n) MODE="online"
		   ;;
		f) FORMULA="${OPTARG}"
		   FORM_SET=1
		   ;;
		a) PHI="${OPTARG}"
		   PHI_SET=1
		   ;; 
		d) DEBUG=1
		   ;;
	esac
done



if [ $FORM_SET -eq 0 ]; then
	echo "Bad arguments
	Mandatory arguments
		1) -n for online mode otherwise offline mode
		2) -f <filename.pml> where filename.pml contains the definition of myformula and other #define
	Optional arguments
		3) -a <filename.pml> where filenam.pml includes the implementation of required interval formulas
		4) -d replay the last falsified property (reproduces the file aux.trail)"
		exit 1;
fi


cat "$FORMULA" > aux.pml

cat "$MODE".pml >>aux.pml

	
if [ $PHI_SET -eq 1 ]; then 
		cat "$PHI" >> aux.pml
fi

COMPILE="-DMEMLIM=512 -O2 trace_manipulation.c"
if [[ "$MODE" == "online" ]]; then
  echo "ONLINE mode compilation"
  COMPILE+=" event_channel.c"
else 
  echo "OFFLINE mode compilation"
fi

if [ $DEBUG -eq 1 ]; then 
	COMPILE+=" -DVERB"
fi

COMPILE+=" -DXUSAFE -DNOCLAIM -w -o pan pan.c" 

echo "$COMPILE"
spin -a  aux.pml 
# if SPIN reports out of memory problems, increase DMEMLIM value
gcc $COMPILE

#if SPIN reports max depth reached, increase de parameter -m
#./pan -m1000 -a -n
# increase max depth -m<value>, usually 1000 is enough in offline and online mode
if [ $DEBUG -eq 0 ]; then
./pan -m1000 -n
else
  ./pan -m1000 -n &> /dev/null
  ./pan -S
fi
