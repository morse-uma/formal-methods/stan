# STAn

STAn is a runtime verification tool for the analysis of system traces implemented on SPIN. STAn is able to check properties described in eLTL.
STAn monitors finite traces (currently provided in cvs file) in two different ways online and offline.

# Installation
The prerequisites to use STAn is to install [SPIN](http://www.spinroot.com/spin/Man/README.html) and include it in the PATH.
Download the files of this repository in your preferred folder, no installation is required.


# Sample formulas
Folder samples contains two examples of eLTL formulas that can be analized using STAn and a sample trace. 
Currently, the traces are provided as two different cvs files, one with the events and another with the periodic measures of system magnitudes.

The file formula1_trace0.pml especifies the necessary definitions and code to analyze trace0 against the property 
    
    Eventually_[stt,stp] True
 
 that determines whether eventually in the trace there is an interval  determined by events _stt_ and _stp_.

 The file formula5_trace0.pml especifies the necessary definitions and code to analyze trace0 against the property 
    
    (Eventually_[stt,stp] rx_data>14 MB ) && Always_[h,l] avg_datarate<=1 MBps

that determines whether more than 14MB have been received in a interval delimited by _stt_ and _stp_ events, and whether the average data rate is below 1Mbps in all intervals determined by _h_ and _l_ events
. 
# Running the sample formulas
STAn.sh is a bash script. It generates the PROMELA model of the eLTL formula and run SPIN to obtain a veredict.

STAn.sh has the following  parameters:

    1. -n : include this parameter to perform online monitoring. Otherwise it performs offline monitoring
    2. -f <formula_file.pml> : the pml file contains the formula to be analyzed (see formula_template.pml) (mandatory)
    3. -a <interval_file.pml> : the pml file contains the implementation of interval formula operators used in your global formula. 
                                  This parameter is optional.
    4. -d : replays the formula_file.pml to show debug information. Suitable when the property is falsified.

For example, to analyze the sample trace against formula 1 using the online monitoring approach:
    stan.h -n -f formula1_trace0.pml

To analyze the sample trace against formula 5 usign the offline monitoring approach:
    stan.h -f formula5_trace0.pml -a phi_offline_formula5
