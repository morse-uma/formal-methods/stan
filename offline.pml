
c_decl{
#include "trace_manipulation.h"
TTrace events,measures;
}

chan rd[FORMULAS] = [0] of {bool, int}; // evaluation, time
chan cm[FORMULAS] = [0] of {int,int};  //ti,te


/***************************************************************************************/
proctype UNTIL_PQ(int id; int c1; int c2; int P; int Q){
	int ti,tf,t,tp,tq;	
	bool result;
end_until_pq: 
    cm[id]?ti,tf;
    t = ti; 
getEvents:     
    c_code{PUNTIL_PQ->tp = nextEvent(events, PUNTIL_PQ->t, PUNTIL_PQ->P);}; 
    if
    :: tp == -1 -> tq= -1;        
    :: else -> c_code{PUNTIL_PQ->tq = nextEvent(events, PUNTIL_PQ->tp+1, PUNTIL_PQ->Q);};
    fi;
    if 
    :: (tq ==-1) || (tp > tf) -> result = false; t = tf; goto  until_pq; //la traza no tiene intervalo
    ::	(tq !=-1)  && (tq <= tf) -> atomic{
    									c_code{printInterval(PUNTIL_PQ->id, "UNTIL_PQ", PUNTIL_PQ->P, PUNTIL_PQ->tp, PUNTIL_PQ->Q, PUNTIL_PQ->tq);}; 
    									cm[c2]!tp,tq;};
    								goto waitC2;  
	fi;
	 
waitC2:
    rd[c2]?result,t; 
    if                    
    :: !result -> t = tq; goto getEvents; 
    :: else -> cm[c1]!ti,tp; 
    fi;
    
waitC1: 
	rd[c1]?result,t;
     
until_pq:
	atomic{	    
		rd[id]!result,t; 
		c_code{printResult(PUNTIL_PQ->id, "UNTIL_PQ", PUNTIL_PQ->result, PUNTIL_PQ->t);};
	};
	   goto end_until_pq;
}

/*****************************************************/
proctype UNTIL_P(int id; int c1; int c2; int P){
	int ti,tf,t,tp;	
	bool result;
end_until_p: 
     cm[id]?ti,tf;
     t = ti; 
     assert(ti>=0 && tf>=0);
getEvents:     
     c_code{PUNTIL_P->tp = nextEvent(events, PUNTIL_P->t, PUNTIL_P->P);};          
     if 
     :: (tp ==-1) || (tp > tf) -> result = false; t = tf; goto  until_p;
     ::	(ti <= tp) && (tp <= tf) -> atomic{
     									c_code{printEvent(PUNTIL_P->id, "UNTIL_P", PUNTIL_P->P, PUNTIL_P->tp);}; 
     									cm[c1]!ti,tp; };
     								goto waitC2;
     :: else -> assert(false);   
	 fi;
     
waitC2:
    rd[c2]?result,t;
    if
    :: !result -> t = tp+1; goto getEvents; 
    :: else -> cm[c1]!tp,tp; t = tp;
    fi;
    
waitC1: 
	rd[c1]?result, t;	   

until_p:
	atomic{   
	   rd[id]!result,t; 
	   c_code{printResult(PUNTIL_P->id, "UNTIL_P", PUNTIL_P->result, PUNTIL_P->t);};
	};
	goto end_until_p;

}


/************************ EVENTUALLY [P,Q]******************************************/
proctype EVENTUALLY_PQ(int id; int c1; int P; int Q){
	int ti,tf,t,tp,tq;	
    bool result;

end_eventually_pq:
     cm[id]?ti,tf;
     t = ti; 
getEvents:     
     c_code{PEVENTUALLY_PQ->tp = nextEvent(events, PEVENTUALLY_PQ->t, PEVENTUALLY_PQ->P);};   
     if
     :: tp ==-1 -> tq=-1;
     :: else -> c_code{PEVENTUALLY_PQ->tq = nextEvent(events, PEVENTUALLY_PQ->tp+1, PEVENTUALLY_PQ->Q);};
	 fi;
	 	 
     if 
     :: (tq ==-1) || (tq > tf) -> result = false; t = tf; goto  eventually_pq;
     ::	(tq != -1) &&(tq <= tf) -> atomic{
     									c_code{printInterval(PEVENTUALLY_PQ->id, "EVENTUALLY_PQ", PEVENTUALLY_PQ->P, PEVENTUALLY_PQ->tp, PEVENTUALLY_PQ->Q, PEVENTUALLY_PQ->tq);}; 
     									cm[c1]!tp,tq;}; 
     								goto waitC1;  
	 fi;

waitC1: 
	rd[c1]?result,t;
	t=tq; 
	if
	:: !result -> goto getEvents; 
	:: else;
	fi;

eventually_pq: 
	atomic{    
		rd[id]!result,t; 
		c_code{printResult(PEVENTUALLY_PQ->id, "EVENTUALLY_PQ", PEVENTUALLY_PQ->result, PEVENTUALLY_PQ->t);};
	};
	goto end_eventually_pq;
}

/******************* EVENTUALLY [P] *******************************************/
proctype EVENTUALLY_P(int id; int c1; int P){
	int ti,tf,t,tp;	
	bool result;
end_eventually_p: 
     cm[id]?ti,tf;
     t = ti; 
getEvents:     
     c_code{PEVENTUALLY_P->tp = nextEvent(events, PEVENTUALLY_P->t, PEVENTUALLY_P->P);};          
     if 
     :: (tp ==-1) || (tp > tf) -> result = false; t =tf; goto  eventually_p;
     ::	(ti <= tp) && (tp <= tf) -> atomic{
     									c_code{printEvent(PEVENTUALLY_P->id, "EVENTUALLY_P", PEVENTUALLY_P->P, PEVENTUALLY_P->tp);}; 
     									cm[c1]!tp,tp;}
     								goto waitC1;
     fi;

waitC1: 
	rd[c1]?result,t;
	if
	:: !result -> t=tp+1; goto getEvents; 
	:: else -> t = tp; goto eventually_p;
	fi;

eventually_p:
	atomic{ 
		rd[id]!result,t; 
		c_code{printResult(PEVENTUALLY_P->id, "EVENTUALLY_P", PEVENTUALLY_P->result, PEVENTUALLY_P->t);};
	}
	goto end_eventually_p;
}


/******************* ALWAYS [P] *******************************************/
proctype ALWAYS_P(int id; int c1; int P){
	int ti,tf,t,tp;	
	bool result;
end_always_p: 
     cm[id]?ti,tf;
     t = ti; 
     assert(ti>=0 && tf>=0);
getEvents:     
     c_code{PALWAYS_P->tp = nextEvent(events, PALWAYS_P->t, PALWAYS_P->P);};          
     if 
     :: (tp ==-1) || (tp > tf) -> result = true; t = tf; goto always_p;
     ::	(ti <= tp) && (tp <= tf) -> atomic{ 
     									c_code{printEvent(PALWAYS_P->id, "ALWAYS_P", PALWAYS_P->P, PALWAYS_P->tp);}; 
     									cm[c1]!tp,tp; };
     								goto waitC1; 
	fi;

waitC1: 
	rd[c1]?result,t;
	if
	:: result -> t=tp+1; goto getEvents; 
	:: else -> t =tp;
	fi;

always_p:  
	atomic{  
	   rd[id]!result,t; 
	   c_code{printResult(PALWAYS_P->id, "ALWAYS_P", PALWAYS_P->result, PALWAYS_P->t);};
	};
	goto end_always_p;
}

/************************ ALWAYS [P,Q]******************************************/
proctype ALWAYS_PQ(int id; int c1; int P; int Q){
	int ti,tf,t,tp,tq;	
	bool result;

end_always_pq:
     cm[id]?ti,tf;
     t = ti; 
     assert(ti>=0 && tf>=0);
getEvents:     
     c_code{PALWAYS_PQ->tp = nextEvent(events, PALWAYS_PQ->t, PALWAYS_PQ->P);}; 
     if  
     :: tp ==-1 -> tq= -1;
     :: else -> c_code{PALWAYS_PQ->tq = nextEvent(events, PALWAYS_PQ->tp+1, PALWAYS_PQ->Q);};
	 fi; 
     if 
     :: (tq ==-1) || (tq > tf) -> result = true; t = tf; goto  always_pq;
     ::	(tq != -1) && (tq <= tf) -> atomic{
     									c_code{printInterval(PALWAYS_PQ->id, "ALWAYS_PQ", PALWAYS_PQ->P, PALWAYS_PQ->tp, PALWAYS_PQ->Q, PALWAYS_PQ->tq);}; 
     									cm[c1]!tp,tq;};    								
     								goto waitC1; 
	 fi;

waitC1: 
	rd[c1]?result,t;
	t=tq; 
	if
	:: result -> goto getEvents; 
	:: else;
	fi;

always_pq:     
	atomic{
		rd[id]!result,t; 
		c_code{printResult(PALWAYS_PQ->id, "ALWAYS_PQ", PALWAYS_PQ->result, PALWAYS_PQ->t);};
	};
	goto end_always_pq;
}

/***************************************************************************************/
proctype OR(int id; int c1; int c2)
{
	int tf,ti,t1,t2;
	bool phi, phiC1, phiC2;	
	bool readyC1, readyC2;

init_or: 
    readyC1= 0; readyC2 = 0;
end_or:
  cm[id]?ti,tf; cm[c1]!ti,tf; cm[c2]!ti,tf;
started_or:	
	do
	:: readyC1 && readyC2 -> goto stopped_or;
	:: rd[c1]?phiC1,t1 -> readyC1 = 1; 
	:: rd[c2]?phiC2,t2 -> readyC2 = 1; 
	od;

stopped_or: 
	phi = phiC1 || phiC2;
	if
	:: !phi -> t1 = (t1<t2 ->t2:t1);
	:: phiC1 && phiC2 -> t1 = (t1<t2 ->t1:t2);
	:: else -> t1 = (phiC1 ->t1:t2) 
	fi;
	atomic{
		rd[id]!phi,t1;
		c_code{printResult(POR->id, "OR", POR->phi, POR->t1);};
	}
	goto init_or;
	
}
/***************************************************************************************/
proctype AND(int id; int c1; int c2)
{
	int tf,ti,t1,t2;
	bool phi, phiC1, phiC2;	
	bool readyC1, readyC2;

init_and: 
    readyC1= 0; readyC2 = 0;
end_and:
  cm[id]?ti,tf; cm[c1]!ti,tf; cm[c2]!ti,tf;
started_and:	
	do
	:: readyC1 && readyC2 -> goto stopped_and;
	:: rd[c1]?phiC1,t1 -> readyC1 = 1; 
	:: rd[c2]?phiC2,t2 -> readyC2 = 1; 
	od;

stopped_and: 
	phi = phiC1 && phiC2;
	if
	:: phi -> t1 = (t1<t2 ->t2:t1);
	:: !phiC1 && !phiC2 -> t1 = (t1<t2 ->t1:t2);
	:: else -> t1 = (!phiC1 ->t1:t2) 
	fi;
	atomic{
		rd[id]!phi,t1;
		c_code{printResult(PAND->id, "AND", PAND->phi, PAND->t1);};
	}
	goto init_and;
	
}
/***************************************************************************************/
proctype NOT(int id; int c1)
{
	int ti,tf,t;
	bool result;
 
end_not:
   cm[id]?ti,tf; 
   cm[c1]!ti,tf;	
	
   rd[c1]?result,t;	
stopped_not: 
    atomic{ 
    	rd[id]!(!result),t;
    	c_code{printResult(PNOT->id, "NOT", PNOT->result, PNOT->t);};
    };
	goto end_not;
	
}
/***************************************************************************************/
proctype TRUE(int id)
{
	int ti, tf;
end_init_true: 
    cm[id]?ti,tf; 
stopped_true:
	atomic{
	  rd[id]!true,tf;
	  c_code{printResult(PTRUE->id, "TRUE", 1, PTRUE->tf);};
	}
	
goto end_init_true;
}

/***************************************************************************************/
proctype PHI_ELAPSED_G(int id; int value)
{
	int ti, tf;
	bool phi;
end_init_phi:cm[id]?ti,tf; 	
	//Time elapsed is greater than value seconds
  c_code{PPHI_ELAPSED_G->phi = events.varTraces[PPHI_ELAPSED_G->tf][0] - events.varTraces[PPHI_ELAPSED_G->ti][0] > (PPHI_ELAPSED_G->value)?1:0;          
         };
stopped_phi:
	rd[id]!phi,tf;
	goto end_init_phi;
}

/**************************************************************************************/
int i;
mtype:event e;
active proctype formula(){
	bool result;
	int t;
	c_code{				
		readTraceCSV(MEASURES_FILE, COLS_M, ROWS_M, &measures);
		readTraceCSV(EVENTS_FILE, COLS_E, ROWS_E, &events);
	}
	atomic{
		myformula();
	};
cm[0]!0,(ROWS_E-1);
init_stop: rd[0]?result,t;	

    if	
    ::result -> c_code{printf("Property SATISFIED at "); };	
    ::else ->  c_code{printf("Property NOT SATISFIED at "); };					 
    fi;
    c_code{ printTime(events.varTraces[Pformula->t][0]);};
    c_code{	destroyTrace(&measures); 
			destroyTrace(&events);
    }; 
	assert(false);
accept_formula:
}
